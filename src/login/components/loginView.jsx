import React, { Component, Fragment } from 'react';
import Banner from "../../shared/assets/crapappbanner.png";
import { routes, regex } from '../../config';
import { FieldControl, FieldGroup, FormBuilder, Validators } from 'react-reactive-form';
import TextInput from '../../shared/components/textInput';
import PasswordInput from '../../shared/components/passwordInput';
import { Redirect } from 'react-router-dom';
import { authenticate } from '../../shared/guards/authentication';
import { loginUser } from '../../shared/services/login';
import { getAllBugs, getBugsFromDB } from '../../shared/services/bugs';
import LoadingScreen from '../../shared/components/loadingScreen';

class LoginView extends Component {
    loginForm = FormBuilder.group({
        username: ['', Validators.compose([
            Validators.required,
        ])],
        password: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.password)
        ])]
    });

    constructor(props) {
        super(props);

        this.state = {
            token: '',
            bugs: []
        }

        getBugsFromDB().then((r) => {
            if (r.status === 200) {
                r.text().then((bugs) => {
                    setTimeout(() => {
                        this.setState({bugs: bugs});
                }, 1);
                    window.sessionStorage.setItem('bugs', bugs);
                });
            }
        });
    }

    render() {
        return (
            this.state.bugs.length === 0? <LoadingScreen /> :
            this.state.token != '' ? <Redirect to={routes.startScreen} /> :
                <Fragment>
                    <FieldGroup
                        control={this.loginForm}
                        render={({ invalid }) => (
                            <form>
                                <div className="container">
                                    <div className="row">
                                        <div className="col s8 offset-s2">
                                            <div className="card">
                                                <div className="card-image">
                                                    <img src={Banner} className="banner" alt="logo crap app" />
                                                </div>
                                                <div className="card-content">
                                                    <div className="row">
                                                        <FieldControl
                                                            name={"username"}
                                                            render={TextInput}
                                                            meta={{ label: getAllBugs()[3].active? "usename" : "Username" }}
                                                        />
                                                    </div>
                                                    <div className="row">
                                                        <FieldControl
                                                            name="password"
                                                            render={PasswordInput}
                                                            meta={{ label: "Password" }}
                                                        />
                                                        <div className="col s12">
                                                            <a className="forgotLink" href={routes.emailForgotPassword}>I Forgot my password</a>
                                                        </div>
                                                        <div className="col s12">
                                                            <a className="forgotLink" href={routes.emailForgotUsername}>I Forgot my username</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card-action">
                                                    <a className="waves-effect waves-light btn"
                                                        disabled={invalid}
                                                        onClick={() => this.login()}
                                                        id="LoginButton"
                                                    >Login</a>
                                                    <p className="registerLinkText">Don't have an account? <a href={routes.register} className="registerLink">Register</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        )
                        } />
                </Fragment>
        );
    }

    login() {
        loginUser(this.loginForm.value).then((response) => {
            if (response.status === 202) {
                response.text().then(() => {
                    authenticate(this.loginForm.value).then((response) => {
                        this.setState({ token: response });
                    });
                });
            } else if (response.status === 404) {
                alert('The specified user does not exist in our system');
            } else if (response.status === 401) {
                alert('The password you specified is wrong');
            } else if (response.status === 403) {
                alert("You can't login, please check your mail for more information");
            }
        });
    }
}

export default LoginView;