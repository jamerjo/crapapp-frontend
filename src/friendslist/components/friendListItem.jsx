import React from "react";
import UserProfile from "../../shared/components/userProfile";


const FriendsListItem = (props) => {
    return (
        <div className="row">
            <table className="col s12 searchTable">
                <tbody className="col s12">
                    <tr className="col s12">
                        <td className="col s7">
                            <UserProfile />
                        </td>
                        <td className="col s4 friendsButtonCell">
                            <a className="waves-effect waves-light btn redButton">UNFRIEND</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

FriendsListItem.propTypes = {

}

export default FriendsListItem;