import React from "react";
import Loading from '../../shared/assets/crapappbanner.png';

const PasswordConfirmLoading = () => {
    return (
        <div className="container centerContainer">
            <div className="row">
                <div className="col s4 offset-s5">
                    <img src={Loading} className="loadingImage" alt="loading"/>
                </div>
            </div>
            <div className="row">
                <div className="col s10 offset-s2">
                    <p>We are checking your request for a new password, please hold on for a moment.</p>
                </div>
            </div>
        </div>
    );
}

export default PasswordConfirmLoading;