import React, { Component } from 'react';
import Toggle from '../../shared/components/toggle';
import { FieldControl, FieldGroup, FormBuilder, Validators } from 'react-reactive-form';
import { getAllBugs, updateBugs } from '../../shared/services/bugs';

class BugsView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            bugs : getAllBugs()
        }
    }

    bugForm = FormBuilder.group({
        FB_PP_EB: [getAllBugs()[0].active, Validators.required],
        FB_AP_UA: [getAllBugs()[1].active, Validators.required],
        BB_LP_WP: [getAllBugs()[2].active, Validators.required],
        FB_LP_SM: [getAllBugs()[3].active, Validators.required]
    });

    render() {
        return (
            <FieldGroup 
            control={this.bugForm}
            render={() => (
                <form>
                    <div className="container">
                    <div className="row">
                    <div className="col s5 offset-s2">
                        <a className="waves-effect waves-light btn logout" onClick={() => {this.saveChanges()}}>SAVE CHANGES</a>
                    </div>
                    </div>
                    <div className="row">
                        <div className="col s8 offset-s2">
                            <div className="card">
                                <div className="card-content">
                                    <div className="row">
                                        <table className="col s10 offset-s2">
                                            <tbody>
                                                <FieldControl
                                                    name={this.state.bugs[0].code}
                                                    render={Toggle}
                                                    meta={{ bugname: `Bug ${this.state.bugs[0].code}`, id: this.state.bugs[0].code }}
                                                />
                                                <FieldControl
                                                    name={this.state.bugs[1].code}
                                                    render={Toggle}
                                                    meta={{ bugname: `Bug ${this.state.bugs[1].code}`, id: this.state.bugs[1].code }}
                                                />
                                                <FieldControl
                                                    name={this.state.bugs[2].code}
                                                    render={Toggle}
                                                    meta={{ bugname: `Bug ${this.state.bugs[2].code}`, id: this.state.bugs[2].code }}
                                                />
                                                <FieldControl
                                                    name={this.state.bugs[3].code}
                                                    render={Toggle}
                                                    meta={{ bugname: `Bug ${this.state.bugs[3].code}`, id: this.state.bugs[3].code }}
                                                />
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                )
            } />
        );
    }

    saveChanges() {
        updateBugs(this.bugForm.value, this.state.bugs).then((r) => {
            if (r.status === 200) {
                getAllBugs();
                alert('your bugs have been updated');
            } else {
                alert('something went wrong');
            }
        })
    }
}

export default BugsView;