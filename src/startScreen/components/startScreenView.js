import React, { Component, Fragment } from 'react';
import UserProfile from '../../shared/components/userProfile';

import { routes } from '../../config';
import { signout } from '../../shared/guards/authentication';
import { getCurrentUser } from '../../shared/services/user';
import LoadingStartScreen from './LoadingStartScreen';
import { getAllBugs } from '../../shared/services/bugs';

class StartScreenView extends Component {

  constructor(props) {
    super(props);
    getAllBugs();

    this.state = {
      user: {
        username: '',
        highscore: '',
      }
    }

    getCurrentUser().then((response) => {
      if (response.status === 200) {
        response.text().then((user) => {
          this.setState({ user: JSON.parse(user)});
        })
      }
    });
  }

  render() {
    return (
      this.state.user.username === '' ? <LoadingStartScreen /> :
        <Fragment>
          <div className="container top">
          {this.state.user.role === 'ADMIN' || getAllBugs()[1].active ?
            <div className="row">
              <div className="col s2 offset-s2">
                <a className="waves-effect waves-light btn logout" href={routes.admin} id="admin">ADMIN</a>
              </div>
            </div> : null}
            <div className="row">
              <div className="col s8 offset-s2">
                <a href={ getAllBugs()[0].active? routes.startScreen : routes.userProfile }>
                  <div className="card horizontal col s12" id="profile">
                    <UserProfile
                      username={this.state.user.username}
                      highscore={this.state.user.highscore}
                    />
                  </div>
                </a>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <a className="waves-effect waves-light btn col s8 offset-s2" >Single player</a>
              </div>
              <div className="row">
                <a className="waves-effect waves-light btn col s8 offset-s2 secondary">Play with friends</a>
              </div>
              <div className="row">
                <a className="logout btn col s8 offset-s2" href={routes.login} onClick={() => signout()}>Logout</a>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col offset-s3">
                  <a className="btn btn-large waves-effect waves-light secondary" href={routes.friendsList} id="friendslist"><i className="material-icons">group</i></a>
                </div>
                <div className="col offset-s2">
                  <a className="btn btn-large waves-effect waves-light" href={routes.searchfriendsList} id="searchfriends"><i className="material-icons">group_add</i></a>
                </div>
              </div>
            </div>
          </div>
        </Fragment >
    );
  }
}

export default StartScreenView;